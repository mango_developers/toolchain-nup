import sys 
import os

N = 16;
file_in_name = sys.argv[1]

file_in = open('%s' % file_in_name, 'r+')
file_out = open(os.path.splitext('%s' % file_in_name)[0] + '_mem.hex', 'wb')
file_out_mango = open(os.path.splitext('%s' % file_in_name)[0] + '_mem_mango.hex', 'wb')

lines = file_in.readlines()
lines = [x.strip("\n") for x in lines]

rest = len(lines)%N
diff = N - rest

lines.extend(["ffffffff"]*diff)
row = len(lines)/N
line_swapped = ""

for j in range(row):
	file_out_mango.write('0x{0:08x}'.format(j*64) + ",0x")
	for i in range(((j+1)*16)-1,(j*16)-1,-1):
		file_out.write(lines[i])
		line_swapped = lines[i][6] + lines[i][7] + lines[i][4] + lines[i][5] + lines[i][2] + lines[i][3] + lines[i][0] + lines[i][1] + line_swapped
	file_out_mango.write(line_swapped)
	line_swapped = ""
	file_out.write("\n")
	file_out_mango.write("\n")

file_in.close()
file_out.close()
